var menu = {
    menu: {
        default: {
            ul_class: 'ul',
            li_class: '',
            a_class: 'a_link',
            level_ul_class: 'subul',
            level_li_class: 'subli',
            level_a_class: 'suba',
            link: [
                {
                    title: 'item-1',
                    active: 'active'
                },
                {title: 'About'},
                {title: 'item-5'},
                {title: 'item-6'},
                {title: 'item-7',
                    level: [
                        {
                            title: 'item-1',
                            active: 'active'
                        },
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7',
                        },
                    ]
                },
            ]
        },
        header: {
            ul_class: 'header_menu',
            li_class: 'header_menu_li',
            a_class: 'header_menu_a',
            level_ul_class: 'header_sub-menu',
            level_li_class: 'header_sub-menu_li',
            level_a_class: 'header_sub-menu_a',
            link: [
                {title: 'item-1'},
                {
                    title: 'item-2',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-3',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-4',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {title: 'item-5'},
                {title: 'item-6'},
                {title: 'item-7'},
                {title: 'item-1'},
                {
                    title: 'item-2',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-3',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-4',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {title: 'item-5'},
                {title: 'item-6'},
                {title: 'item-7'},
                {title: 'item-1'},
                {
                    title: 'item-2',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-3',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-4',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {title: 'item-5'},
                {title: 'item-6'},
                {title: 'item-7'},
                {title: 'item-1'},
                {
                    title: 'item-2',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-3',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {
                    title: 'item-4',
                    active: 'active',
                    level: [
                        {title: 'item-1'},
                        {
                            title: 'item-2',
                            active: 'active'
                        },
                        {title: 'item-3'},
                        {title: 'item-4'},
                        {title: 'item-5'},
                        {title: 'item-6'},
                        {title: 'item-7'},
                    ],
                },
                {title: 'item-5'},
                {title: 'item-6'},
                {title: 'item-7'},
            ]
        }
    }
};

